import 'package:flutter/material.dart';

import '../layouts/image_placeholder.dart';

class Login extends StatelessWidget {
  const Login({
    super.key, 
    this.phoneController, 
    this.passwordController});

  final TextEditingController? phoneController ;
  final TextEditingController? passwordController;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.topCenter,
                child: ListView(
                  shrinkWrap: true,
                  // restorationId: 'login_id',
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  children: [
                   const _SmallLogo(),
                    PhoneInput(
                      phoneController: phoneController,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _SmallLogo extends StatelessWidget {
  const _SmallLogo();

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 64),
      child: SizedBox(
        height: 160,
        child: ExcludeSemantics(
          child: FadeImageProvider(
            image: AssetImage('assets/avatar/avatar.png'),
            placeholder: SizedBox.shrink(),
          ),
        ),
      ),
    );
  }
}

class PhoneInput extends StatelessWidget {
  const PhoneInput({super.key, this.maxWidth, this.phoneController});

  final double? maxWidth;
  final TextEditingController? phoneController;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        constraints: BoxConstraints(maxWidth:  maxWidth ?? double.infinity),
        child: TextField(
          controller: phoneController,
          autofillHints: const [AutofillHints.telephoneNumber],
          textInputAction: TextInputAction.next,
          decoration: const InputDecoration(labelText: 'Phone'),
        ),
      ),
    );
  }
}
